#include "stm32Lib.h"
#include <stdlib.h>
#include <stdint.h>
/*
 * utils.h
 *
 *  Created on: 03.12.2017
 *      Author: KC
 */

#ifndef UTILS_H_
#define UTILS_H_

enum parserState{
	IDLE,
	PROCESSING,
	READY,
	ERR
};

void stringParser(char* str, uint8_t length, char nextC, void (*handler)(char*));

#endif /* UTILS_H_ */
