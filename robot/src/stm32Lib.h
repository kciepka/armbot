#include "stm32f10x.h"
#include <stdio.h>

/*
 * stm32Lib.h
 *
 *  Created on: 02.12.2017
 *      Author: KC
 */

#ifndef STM32LIB_H_
#define STM32LIB_H_

enum pinState{
	LOW,
	HIGH
};

enum pinDirection{
	INPUT,
	OUTPUT,
	OUTPUT_AF
};

// SYS TICK
void setupTickMs();
uint32_t getTickMs();

// GPIO
void setupPin(GPIO_TypeDef * port, uint16_t pin, enum pinDirection direction);
enum pinState getPinState(GPIO_TypeDef * port, uint16_t pin);
void setPinState(GPIO_TypeDef * port, uint16_t pin, enum pinState state);

// TIMERS
void setupTimer1();
void enableTimer1();
void disableTimer1();
void setupTimer1PWMChannel1(uint8_t impulseWidth);
void setupTimer1PWMChannel2(uint8_t impulseWidth);
void setupTimer1PWMChannel3(uint8_t impulseWidth);
void setupTimer1PWMChannel4(uint8_t impulseWidth);
void setupTimer1PWMChannel1Callback(void (*callback)());
void setupTimer1PWMChannel2Callback(void (*callback)());
void setupTimer1PWMChannel3Callback(void (*callback)());
void setupTimer1PWMChannel4Callback(void (*callback)());

void setupTimer2(uint16_t ms);
void enableTimer2();
void disableTimer2();
void setupTimer2Callback(void (*callback)());

// UART
void setupUart2(uint32_t baudrate);
void enableUart2();
void disableUart2();
void setupUart2Callback(void (*handler)(char));
void sendUartChar(char c);
void sendUart(const char* s);

// EXTI
void setupExternalInterrupt0(GPIO_TypeDef * port, uint16_t pin);
void setupExternalInterrupt0Callback(void (*callback)());

void setupExternalInterrupt1(GPIO_TypeDef * port, uint16_t pin);
void setupExternalInterrupt1Callback(void (*callback)());

void setupExternalInterrupt2(GPIO_TypeDef * port, uint16_t pin);
void setupExternalInterrupt2Callback(void (*callback)());

#endif /* STM32LIB_H_ */
