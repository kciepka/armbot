#include <stdlib.h>
#include <string.h>
#include "stm32Lib.h"
#include "L298NLib.h"

 char* command;

 void handleRemoteCommand(char* command)
 {
	 if(strcmp(command, "f") == 0){
		 forward();
	 }
	 else if(strcmp(command, "l") == 0){
		 left();
	 }
	 else if(strcmp(command, "r") == 0){
		 right();
	 }
	 else if(strcmp(command, "b") == 0){
		 reverse();
	 }
	 else if(strcmp(command, "s") == 0){
		 stop();
	 }
 }

 void turnOnLights(){
 	 setupPin(GPIOA, GPIO_Pin_9, OUTPUT);
 	 setPinState(GPIOA, GPIO_Pin_9, HIGH);
 }

 void turnOffLights(){
 	 setupPin(GPIOA, GPIO_Pin_9, OUTPUT);
 	 setPinState(GPIOA, GPIO_Pin_9, LOW);
 }

 void enableBuzzer(){
 	 setupPin(GPIOA, GPIO_Pin_10, OUTPUT);
 	 setPinState(GPIOA, GPIO_Pin_10, HIGH);
 }

 void disableBuzzer(){
 	 setupPin(GPIOA, GPIO_Pin_10, OUTPUT);
 	 setPinState(GPIOA, GPIO_Pin_10, LOW);
 }

 void uartHandler(char c){
	 stringParser(command, 10, c, &handleRemoteCommand);
 }

int main(void)
{
//	init();
//	command = (char*) malloc(10);
//	setupUart2InterruptHandler(&uartHandler);
//	enableUart2(115200);
//	setupExternalInterrupt0();
//	setupExternalInterrupt1();
//	setupExternalInterrupt2();
	setupL298N(GPIOA, RCC_APB2Periph_GPIOA, GPIO_Pin_4, GPIO_Pin_5, GPIO_Pin_6, GPIO_Pin_7);

	while (1) {
	}
}

