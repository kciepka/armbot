#include "L298NLib.h"
/*
 * L298NLib.c
 *
 *  Created on: 02.12.2017
 *      Author: KC
 */

 static GPIO_TypeDef * EnginePort;
 static uint16_t IN1;
 static uint16_t IN2;
 static uint16_t IN3;
 static uint16_t IN4;

void setupL298N( GPIO_TypeDef * port, uint32_t clockPort, uint16_t pinIN1, uint16_t pinIN2, uint16_t pinIN3, uint16_t pinIN4 ){
	EnginePort = port;
	IN1 = pinIN1;
	IN2 = pinIN2;
	IN3 = pinIN3;
	IN4 = pinIN4;

	RCC_APB2PeriphClockCmd(clockPort, ENABLE);

	setupPin(EnginePort, IN1, OUTPUT);
	setupPin(EnginePort, IN2, OUTPUT);
	setupPin(EnginePort, IN3, OUTPUT);
	setupPin(EnginePort, IN4, OUTPUT);

	setPinState(EnginePort, IN1, LOW);
	setPinState(EnginePort, IN2, LOW);
	setPinState(EnginePort, IN3, LOW);
	setPinState(EnginePort, IN4, LOW);
}

void forward(){
	stop();
	setPinState(EnginePort, IN2, HIGH);
	setPinState(EnginePort, IN1, LOW);
	setPinState(EnginePort, IN4, HIGH);
	setPinState(EnginePort, IN3, LOW);
}

void reverse(){
	stop();
	setPinState(EnginePort, IN1, HIGH);
	setPinState(EnginePort, IN2, LOW);
	setPinState(EnginePort, IN3, HIGH);
	setPinState(EnginePort, IN4, LOW);
}

void left(){
	stop();
	setPinState(EnginePort, IN3, HIGH);
	setPinState(EnginePort, IN4, LOW);
	setPinState(EnginePort, IN2, HIGH);
	setPinState(EnginePort, IN1, LOW);
}

void right(){
	stop();
	setPinState(EnginePort, IN4, HIGH);
	setPinState(EnginePort, IN3, LOW);
	setPinState(EnginePort, IN1, HIGH);
	setPinState(EnginePort, IN2, LOW);
}

void stop(){
	setPinState(EnginePort, IN1, LOW);
	setPinState(EnginePort, IN2, LOW);
	setPinState(EnginePort, IN3, LOW);
	setPinState(EnginePort, IN4, LOW);
}

