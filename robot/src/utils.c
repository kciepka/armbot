#include "utils.h"
/*
 * utils.c
 *
 *  Created on: 03.12.2017
 *      Author: KC
 */


void stringParser(char* str, uint8_t length, char nextC, void (*handler)(char*)){
	 static enum parserState state = IDLE;
	 uint8_t lastIndex = length - 1;

	 if(state == IDLE || state == READY || state == ERR){
		 free(str);
		 str = malloc(length);
		 str[0] = nextC;
		 str[1] = '\0';
		 state = PROCESSING;
		 return;
	 }

	 if(state == PROCESSING){
		 if(nextC == '\n'){
			 (*handler)(str);
			 state = READY;
			 return;
		 }

		 int i = 0;
		 while(str[i] != '\0') i++;
		 if(i+1 > lastIndex) {
			 state = ERR;
			 return;
		 }
		 str[i] = nextC;
		 str[i+1] = '\0';
		 return;
	 }
 }
