#include "stm32Lib.h"

/*
 * stm32Lib.c
 *
 *  Created on: 02.12.2017
 *      Author: KC
 */


// GENERAL

static volatile uint32_t msTick = 0;

static uint32_t getAPBFromPort(GPIO_TypeDef * port){
	if(port ==  GPIOA) return RCC_APB2Periph_GPIOA;
	else if(port == GPIOB) return RCC_APB2Periph_GPIOB;
	else if(port == GPIOC) return RCC_APB2Periph_GPIOC;
	else if(port == GPIOD) return RCC_APB2Periph_GPIOD;
	else return 0;
}

static uint8_t getPortSourceFromPort(GPIO_TypeDef * port){
	if(port ==  GPIOA) return GPIO_PortSourceGPIOA;
	else if(port == GPIOB) return GPIO_PortSourceGPIOB;
	else if(port == GPIOC) return GPIO_PortSourceGPIOC;
	else if(port == GPIOD) return GPIO_PortSourceGPIOD;
	else return 0;
}

static uint8_t getPinSourceFromPin(uint16_t pin){
	if(pin == GPIO_Pin_0) return GPIO_PinSource0;
	else if(pin == GPIO_Pin_1) return GPIO_PinSource1;
	else if(pin == GPIO_Pin_2) return GPIO_PinSource2;
	else if(pin == GPIO_Pin_3) return GPIO_PinSource3;
	else if(pin == GPIO_Pin_4) return GPIO_PinSource4;
	else if(pin == GPIO_Pin_5) return GPIO_PinSource5;
	else if(pin == GPIO_Pin_6) return GPIO_PinSource6;
	else if(pin == GPIO_Pin_7) return GPIO_PinSource7;
	else if(pin == GPIO_Pin_8) return GPIO_PinSource8;
	else if(pin == GPIO_Pin_9) return GPIO_PinSource9;
	else if(pin == GPIO_Pin_10) return GPIO_PinSource10;
	else if(pin == GPIO_Pin_11) return GPIO_PinSource11;
	else if(pin == GPIO_Pin_12) return GPIO_PinSource12;
	else if(pin == GPIO_Pin_13) return GPIO_PinSource13;
	else if(pin == GPIO_Pin_14) return GPIO_PinSource14;
	else if(pin == GPIO_Pin_15) return GPIO_PinSource15;
	else return 0;
}

// SYS TICK

void SysTick_Handler()
{
	msTick++;
}

uint32_t getTickMs(){
	return msTick;
}

void setupTickMs(){
	SysTick_Config(SystemCoreClock / 1000);
}

// GPIO

void setupPin(GPIO_TypeDef * port, uint16_t pin, enum pinDirection direction){
	GPIO_InitTypeDef gpio;

	GPIO_StructInit(&gpio);
	gpio.GPIO_Pin = pin;
	gpio.GPIO_Speed = GPIO_Speed_50MHz;
	if(direction == OUTPUT){
		gpio.GPIO_Mode = GPIO_Mode_Out_PP;
	}
	else if(direction == OUTPUT_AF){
		gpio.GPIO_Mode = GPIO_Mode_AF_PP;
	}
	else {
		gpio.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	}
	GPIO_Init(port, &gpio);
}

enum pinState getPinState(GPIO_TypeDef * port, uint16_t pin){
	return GPIO_ReadInputDataBit(port, pin) == Bit_SET ? HIGH : LOW;
}

void setPinState(GPIO_TypeDef * port, uint16_t pin, enum pinState state){
	(state == HIGH) ? GPIO_SetBits(port, pin) : GPIO_ResetBits(port, pin);
}

// UART

void setupUart2(uint32_t baudrate){
	 USART_InitTypeDef uart;
	 NVIC_InitTypeDef nvic;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	setupPin(GPIOA, GPIO_Pin_2, OUTPUT_AF);
	setupPin(GPIOA,  GPIO_Pin_3, INPUT);

	 nvic.NVIC_IRQChannel = USART2_IRQn;
	 nvic.NVIC_IRQChannelCmd = ENABLE;
	 nvic.NVIC_IRQChannelPreemptionPriority = 0;
	 nvic.NVIC_IRQChannelSubPriority = 0;
	 NVIC_Init(&nvic);

	 USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

	 USART_StructInit(&uart);
	 uart.USART_BaudRate = baudrate;
	 uart.USART_WordLength = USART_WordLength_8b;
	 uart.USART_StopBits = USART_StopBits_1;
	 uart.USART_Parity = USART_Parity_No;
	 uart.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	 uart.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	 USART_Init(USART2, &uart);
}

void enableUart2(){
	USART_Cmd(USART2, ENABLE);
}

void disableUart2(){
	USART_Cmd(USART2, DISABLE);
}

void sendUart2Char(char c)
{
	while (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET);
	USART_SendData(USART2, c);
}

void sendUart2(const char* s)
{
	while (*s) sendUart2Char(*s++);
}

static void (*uart2Callback)(char);

void setupUart2Callback(void (*callback)(char)){
	uart2Callback = callback;
}

void USART2_IRQHandler(void) {
    if (USART_GetITStatus(USART2, USART_IT_RXNE)) {
    	char c = USART_ReceiveData(USART2);
    	(*uart2Callback)(c);
        USART_ClearITPendingBit(USART2, USART_IT_RXNE);
    }
}

// TIMERS

//u16 capture = 0;
vu16 CC1; //volatile uint16_t
vu16 CC2;
vu16 CC3;
vu16 CC4;

static void (*timer1CC1Callback)();
static void (*timer1CC2Callback)();
static void (*timer1CC3Callback)();
static void (*timer1CC4Callback)();

void setupTimer1PWMChannel1Callback(void (*callback)()){
	timer1CC1Callback = callback;
}

void setupTimer1PWMChannel2Callback(void (*callback)()){
	timer1CC2Callback = callback;
}

void setupTimer1PWMChannel3Callback(void (*callback)()){
	timer1CC3Callback = callback;
}

void setupTimer1PWMChannel4Callback(void (*callback)()){
	timer1CC4Callback = callback;
}

void TIM1_CC_IRQHandler(void)
{
	if (TIM_GetITStatus(TIM1, TIM_IT_CC1) != RESET)
	{
		(*timer1CC1Callback)();
		TIM_ClearITPendingBit(TIM1, TIM_IT_CC1);
		TIM_SetCompare1(TIM1, TIM_GetCapture1(TIM1) + CC1);
	}
	else if (TIM_GetITStatus(TIM1, TIM_IT_CC2) != RESET)
	{
		(*timer1CC2Callback)();
		TIM_ClearITPendingBit(TIM1, TIM_IT_CC2);
		TIM_SetCompare2(TIM1, TIM_GetCapture2(TIM1) + CC2);
	}
	else if (TIM_GetITStatus(TIM1, TIM_IT_CC3) != RESET)
	{
		(*timer1CC3Callback)();
		TIM_ClearITPendingBit(TIM1, TIM_IT_CC3);
		TIM_SetCompare3(TIM1, TIM_GetCapture3(TIM1) + CC3);
	}
	else
	{
		(*timer1CC4Callback)();
		TIM_ClearITPendingBit(TIM1, TIM_IT_CC4);
		TIM_SetCompare4(TIM1, TIM_GetCapture4(TIM1) + CC4);
	}
}

void setupTimer1(){
	TIM_TimeBaseInitTypeDef tim;
	NVIC_InitTypeDef nvic;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);

	nvic.NVIC_IRQChannel = TIM1_CC_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = 0;
	nvic.NVIC_IRQChannelSubPriority = 1;
	nvic.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic);

	tim.TIM_Period = 65535;
	tim.TIM_Prescaler = 1440;
	tim.TIM_ClockDivision = 0;
	tim.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM1, &tim);
}

void setupTimer1PWMChannel1(uint8_t impulseWidth){

	if(impulseWidth > 100) impulseWidth = 100;
	uint8_t divider = 100/impulseWidth;

	CC1 = 65536/divider;

	TIM_OCInitTypeDef timOC;
	timOC.TIM_OCMode = TIM_OCMode_Timing;
	timOC.TIM_OutputState = TIM_OutputState_Enable;
	timOC.TIM_Pulse = CC1;
	timOC.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC1Init(TIM1, &timOC);

	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Disable);
}

void setupTimer1PWMChannel2(uint8_t impulseWidth){
	if(impulseWidth > 100) impulseWidth = 100;
	uint8_t divider = 100/impulseWidth;

	CC2 = 65536/divider;

	TIM_OCInitTypeDef timOC;
	timOC.TIM_OCMode = TIM_OCMode_Timing;
	timOC.TIM_OCPolarity = TIM_OCPolarity_High;
	timOC.TIM_OutputState = TIM_OutputState_Enable;
	timOC.TIM_Pulse = CC2;
	TIM_OC2Init(TIM1, &timOC);

	TIM_OC2PreloadConfig(TIM1, TIM_OCPreload_Disable);
}

void setupTimer1PWMChannel3(uint8_t impulseWidth){
	if(impulseWidth > 100) impulseWidth = 100;
	uint8_t divider = 100/impulseWidth;

	CC3 = 65536/divider;

	TIM_OCInitTypeDef timOC;
	timOC.TIM_OCMode = TIM_OCMode_Timing;
	timOC.TIM_OCPolarity = TIM_OCPolarity_High;
	timOC.TIM_OutputState = TIM_OutputState_Enable;
	timOC.TIM_Pulse = CC3;
	TIM_OC3Init(TIM1, &timOC);

	TIM_OC3PreloadConfig(TIM1, TIM_OCPreload_Disable);
}

void setupTimer1PWMChannel4(uint8_t impulseWidth){
	if(impulseWidth > 100) impulseWidth = 100;
	uint8_t divider = 100/impulseWidth;

	CC4 = 65536/divider;

	TIM_OCInitTypeDef timOC;
	timOC.TIM_OCMode = TIM_OCMode_Timing;
	timOC.TIM_OCPolarity = TIM_OCPolarity_High;
	timOC.TIM_OutputState = TIM_OutputState_Enable;
	timOC.TIM_Pulse = CC4;
	TIM_OC4Init(TIM1, &timOC);

	TIM_OC4PreloadConfig(TIM1, TIM_OCPreload_Disable);
}

void enableTimer1(){
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
	TIM_ITConfig(TIM1, TIM_IT_CC1 | TIM_IT_CC2 | TIM_IT_CC3 | TIM_IT_CC4, ENABLE);
	TIM_Cmd(TIM1, ENABLE);
}

void disableTimer1(){
	TIM_ITConfig(TIM1, TIM_IT_CC1 | TIM_IT_CC2 | TIM_IT_CC3 | TIM_IT_CC4, DISABLE);
	TIM_Cmd(TIM1, DISABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, DISABLE);
}

void setupTimer2(uint16_t ms){
	TIM_TimeBaseInitTypeDef tim;
	NVIC_InitTypeDef nvic;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	TIM_TimeBaseStructInit(&tim);
	tim.TIM_CounterMode = TIM_CounterMode_Up;
	tim.TIM_Prescaler = 64000 - 1;
	tim.TIM_Period = ms > 0 ? ms -1 : 0;
	TIM_TimeBaseInit(TIM2, &tim);

	nvic.NVIC_IRQChannel = TIM2_IRQn;
	nvic.NVIC_IRQChannelPreemptionPriority = 0;
	nvic.NVIC_IRQChannelSubPriority = 0;
	nvic.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&nvic);
}

void enableTimer2(){
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM2, ENABLE);
}

void disableTimer2(){
	TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);
	TIM_Cmd(TIM2, DISABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, DISABLE);
}

static void (*timer2Callback)();

void setupTimer2Callback(void (*callback)()){
	timer2Callback = callback;
}

void TIM2_IRQHandler()
{
	 if (TIM_GetITStatus(TIM2, TIM_IT_Update) == SET)
	 {
		 (*timer2Callback)();
		 TIM_ClearITPendingBit(TIM2, TIM_IT_Update);
	 }
}

// EXTI

void setupExternalInterrupt0(GPIO_TypeDef * port, uint16_t pin) {
    GPIO_InitTypeDef gpio;
    EXTI_InitTypeDef exti;
    NVIC_InitTypeDef nvic;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
    RCC_APB2PeriphClockCmd(getAPBFromPort(port), ENABLE);

    gpio.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    gpio.GPIO_Pin = pin;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(port, &gpio);

    GPIO_EXTILineConfig(getPortSourceFromPort(port), getPinSourceFromPin(pin));
    EXTI_ClearITPendingBit(EXTI_Line0);

    exti.EXTI_Line = EXTI_Line0;
    exti.EXTI_LineCmd = ENABLE;
    exti.EXTI_Mode = EXTI_Mode_Interrupt;
    exti.EXTI_Trigger = EXTI_Trigger_Rising;
    EXTI_Init(&exti);

    nvic.NVIC_IRQChannel = EXTI0_IRQn;
    nvic.NVIC_IRQChannelPreemptionPriority = 0x00;
    nvic.NVIC_IRQChannelSubPriority = 0x00;
    nvic.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvic);
}

static void (*externalInterrupt0Callback)();

void setupEternalInterrupt0Callback(void (*callback)()){
	externalInterrupt0Callback = callback;
}

void EXTI0_IRQHandler(void) {
    if (EXTI_GetITStatus(EXTI_Line0) != RESET) {
    	(*externalInterrupt0Callback)();
        EXTI_ClearITPendingBit(EXTI_Line0);
    }
}

void setupExternalInterrupt1(GPIO_TypeDef * port, uint16_t pin) {
    GPIO_InitTypeDef gpio;
    EXTI_InitTypeDef exti;
    NVIC_InitTypeDef nvic;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
    RCC_APB2PeriphClockCmd(getAPBFromPort(port), ENABLE);

    gpio.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    gpio.GPIO_Pin = pin;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(port, &gpio);

    GPIO_EXTILineConfig(getPortSourceFromPort(port), getPinSourceFromPin(pin));
    EXTI_ClearITPendingBit(EXTI_Line1);

    exti.EXTI_Line = EXTI_Line1;
    exti.EXTI_LineCmd = ENABLE;
    exti.EXTI_Mode = EXTI_Mode_Interrupt;
    exti.EXTI_Trigger = EXTI_Trigger_Rising;
    EXTI_Init(&exti);

    nvic.NVIC_IRQChannel = EXTI1_IRQn;
    nvic.NVIC_IRQChannelPreemptionPriority = 0x00;
    nvic.NVIC_IRQChannelSubPriority = 0x01;
    nvic.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvic);
}

static void (*externalInterrupt1Callback)();

void setupEternalInterrupt1Callback(void (*callback)()){
	externalInterrupt1Callback = callback;
}

void EXTI1_IRQHandler(void) {
    if (EXTI_GetITStatus(EXTI_Line1) != RESET) {
    	(*externalInterrupt1Callback)();
        EXTI_ClearITPendingBit(EXTI_Line1);
    }
}

void setupExternalInterrupt2(GPIO_TypeDef * port, uint16_t pin) {
    GPIO_InitTypeDef gpio;
    EXTI_InitTypeDef exti;
    NVIC_InitTypeDef nvic;

    RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
    RCC_APB2PeriphClockCmd(getAPBFromPort(port), ENABLE);

    gpio.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    gpio.GPIO_Pin = pin;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(port, &gpio);

    GPIO_EXTILineConfig(getPortSourceFromPort(port), getPinSourceFromPin(pin));
    EXTI_ClearITPendingBit(EXTI_Line2);

    exti.EXTI_Line = EXTI_Line2;
    exti.EXTI_LineCmd = ENABLE;
    exti.EXTI_Mode = EXTI_Mode_Interrupt;
    exti.EXTI_Trigger = EXTI_Trigger_Rising;
    EXTI_Init(&exti);

    nvic.NVIC_IRQChannel = EXTI2_IRQn;
    nvic.NVIC_IRQChannelPreemptionPriority = 0x00;
    nvic.NVIC_IRQChannelSubPriority = 0x02;
    nvic.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvic);
}

static void (*externalInterrupt2Callback)();

void setupEternalInterrupt2Callback(void (*callback)()){
	externalInterrupt2Callback = callback;
}

void EXTI2_IRQHandler(void) {
    if (EXTI_GetITStatus(EXTI_Line2) != RESET) {
    	(*externalInterrupt2Callback)();
        EXTI_ClearITPendingBit(EXTI_Line2);
    }
}

