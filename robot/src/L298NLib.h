#include "stm32Lib.h"
#include "utils.h"

/*
 * L298NLib.h
 *
 *  Created on: 02.12.2017
 *      Author: KC
 */

#ifndef L298NLIB_H_
#define L298NLIB_H_

void setupL298N( GPIO_TypeDef * port, uint32_t clockPort, uint16_t pinEN1, uint16_t pinEN2, uint16_t pinEN3, uint16_t pinEN4 );
void forward();
void reverse();
void left();
void right();
void stop();

#endif /* L298NLIB_H_ */
